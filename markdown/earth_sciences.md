# Earth Sciences questions

## Geology

### Geological structure of Earth

* asthenosphere
* crust, mantle
* 3000 kilometers below the surface of the earth

### Geochronology

* Jurassic Period
* How long did the Paleoproterozoic Era last?
* What did the world look like during the Cambrian Period?
* continental map for the Karoo ice age
* Tonian Period parent division
* important geological events during the Permian Period
* important biological events of the Late Triassic
* oxygen levels during the Jurassic
* last glaciation
* biodiversity during the Lopingian Epoch
* 20 million years ago

### Impact craters on Earth

* Beaverhead Crater
* Vredefort Crater, Morokweng Crater
* age Sierra Madera Crater
* Vredefort Crater elevation map
* Sudbury Crater diameter / Denali height

## Geodesy & Navigation

### Latitude & Logitude

* 55 deg 45' N, 37 deg 37' E
* Algiers coordinates

### Distances

* 23.5S 46.4W to 56.8N 60.6E
* San Francisco to Tokyo
* Buenos Aires to Sao Paulo to La Paz to Panama
* how far can I see at 30,000 ft
* calculate line of sight to the horizon from Mount Everest
* how far can radio waves propagate
* calculate radio horizon at 100 m altitude

### Bearings

* SSW
* 15 North

### Geodetic Data

* GRS80
* Airy 1830, Modified Fischer 1960
* GDA94
* NAD27, ITRF00

### Geogravity

* geogravity in Seattle
* geogravity 21.28N 157.60W

### Geomagnetism

* geomagnetism Oklahoma City
* geomagnetism 43.7N 45.2E
* geomagnetism Oslo on 12 Feb 2007

## Volcanoes

### Volcanoes

* Irruputuncu
* Krakatau, Tambora
* last eruption of Tori-Shima
* height of Gamchen volcano

### Volcanic Eruptions

* volcanic eruptions
* most destructive volcanic eruption
* volcanic eruptions during the second world war
* volcanic eruptions in sicily in the 1970s

## Earthquakes

### Earthquake Data

* earthquakes near Lima
* earthquakes Mexico 1985
* earthquakes in the US in the last week
* earthquakes near Tokyo last ten years on local map

### Earthquake Magnitudes

* Richter scale 6
* find definitions of magnitudes
* define body wave magnitude
* Japan Meteorological Agency magnitude definition

### Earthquake Intensity

* earthquake intensity scales
* Mercalli scale
* Liedu scale

### Earthquake Characteristics

* ground motion prediction moment magnitude 6.2
* calculate earthquake characteristics seismic moment magnitude 7
* fault slip rate

### Seismic Travel Times

* seismic travel times from San Francisco to Los Angeles
* distance from seismic wave travel times

### Earthquake Visualizations

* focal mechanism 2d
* styles of faulting

## Oceanography

### Oceans

* Atlantic Ocean
* Indian Ocean, Southern Ocean

### Properties of Oceans

* lowest point of the North Sea
* area Pacific Ocean / Atlantic Ocean

### Seawater Characteristics

* ocean at 300m
* sound speed in the ocean at 1600ft
* ocean at 25000ft, 40F, 33psu
* sound absorption in sea water
* what is the absorption coefficient of sound in the atlantic ocean?
* freezing point of seawater

### Tides

* tides Seattle
* low tide Hong Kong
* tides Oahu tomorrow
* tides Boston October 2005

## Atmospheric sciences

### Standard Atmosphere

* atmosphere 40000ft
* sound speed at 100,000ft

### Atmospheric Layers

* atmospheric layers
* mesosphere
* 50000 feet above the earth

### Atmospheric Composition

* atmosphere of Earth

### Barometric Pressure

* barometric pressure at extreme altitudes
* What is the barometric pressure on top of K2?

### Raindrops

* 5mm diameter rain drop
* time to fall for 2mm raindrop
* speed of 1mm raindrop
* average rain drop size in 20 mm/hr rain

### Carbon Footprints

* carbon footprint coal
* carbon footprint natural gas
* carbon footprint coal 100W
* 23W petroleum carbon footprint
* carbon footprint gas 15000 Btu/h
* carbon footprint driving 536 miles at 32mpg
* global warming potential of methane
* global warming potential of CF4

### Fluid Mechanics

* Bernoulli's equation
* archimedes principle, 2.2 cubic feet oak wood in water
* Venturi flowmeter
* velocity of water hammer with pressure difference of 20 atm
* hydraulic shock pressure in 5 s
* flow around a cylinder
* Blasius displacement thickness
* Rossiter formula for mach 2
* vortex potential
* p0=1 bar, height = 3km
* rho=1g/cm^3, h=3m, atmospheric pressure=1 bar, p=?
* NACA 3412 airfoil, 12deg
* true airspeed = 550 knots, altitude = 35000 ft
* compute true airspeed given p0=22inHg, 20000ft
* Mach 0.7 @ 45,000ft
* slant range to aircraft 40deg, altitude=35000ft
* slant range to aircraft, look angle=20 deg
* Eckert number
* Brinkman rheological number formula
* Boussinesq approximation parameter formula
* Arrhenius number 300C, 84kJ/mol
* Galileo number L=350um, nu=42cSt

### Electromagnetic Radiation

* absorption of em waves at 30 C
