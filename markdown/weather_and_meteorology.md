# Weather & Meteorology questions

## Current weather

* high temperature for today
* weather in Sevastopol
* rainfall in Mumbai
* wind direction in Ankara
* ветер в Москве

## Weather history

* weather Kiev 15 november 2002
* weather tokyo vs helsinki 2000 to 2017
* погода Краснодар 15 ноября 2002
* погода севастополь за последние 5 лет

## Weather forecasts

* weather forecast for 7 days Sevastopol
* прогноз погоды завтра Камерун

## Sunburn & UV index

* 3 hours in the sun with SPF 15 in Kabul
* 2 часа на солнце в Киеве

## Atmospheric thermodynamics

### Wind chill & heat index

* wind chill, 25 degrees C, 30kmph
* heat index, 33 degrees C

### Psychrometrics

* moist air drybulb 72F, wetbulb 66F, pressure 29inHg
* dew point at drybulb 30C and at 6000ft
* humidity ratio at 28C, 2atm
* humidity ratio of moist air at 86F, 37inHg
* what is the absolute humidity of moist air at 0.87 atm 32C?
* calculate specific humidity at 10C, 30 percent
* what is the mass of water in 30 cubic feet of moist air?
* absorption coefficient of sound moist air at 30C and 20 percent and 6000ft
* adiabatically cool moist air at Tdry=30C to Tdry2=22C
* humidify moist air at drybulb 15C and 20 percent to 30C and 55 percent
* adiabatically mix moist air at drybulb 20C wetbulb 10C with Tdry2=35C Twet2=30C

### Heating & cooling degree days

* degree days Paris
* heating degree days in Buffalo
* degree days June 2009 to October 2009 in Los Angeles

### Saturation vapor pressure

* saturation vapor pressure 30C

## Frostbite

* frostbite, -10 degrees F, 25 kmph

## Tropical storms

### Tropical storms

* Cyclone Tracy, Hurricane Isidore, Typhoon Songda
* Hurricane Pauline 1997 and Cyclone Wanda 1974
* Cyclones in 1974, Hurricanes in 1991, Typhoons in 2004
* Hurricane Katrina, Hurricane Rita
* Typhoon Durian 2006, Typhoon Angela 1995

### Properties of tropical storms

* Hurricane Beth max wind speed
* When did Typhoon Angela start?
* Hurricane Andrew, Hurricane Hugo duration

## US tornadoes

* tornadoes in 2010
* tornadoes near LA
* F4 tornadoes in Indiana
* F5 tornadoes after 1950 in Alabama
* tornadoes within 40 miles of St. Louis since 2010

## Clouds

### Classification of clouds

* cirrus
* nimbostratus, cumulus

### Cloud base height

* cloud base height
* altocumulus, cumulus cloud height

### Cloud cover

* cloud cover Beijing
* cloud cover in Russia vs Mongolia May 21, 2011

## Climate

* global climate studies
* global CO2 level
* global methane level
* climate studies Northern Hemisphere
* Pacific ocean climate
* Southern Oscillation Index
* England climate 1600 to 1900
* global climate study Chuine et al 2004
