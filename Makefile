all: install prod

# Указываем наш APP_ID
APP_ID=X7A9EJ-7A9TJ5GHL2
SECRET_KEY=$(shell head /dev/urandom | tr -dc A-Za-z0-9 | head -c 20 ; echo '')

# Запускаем на проде 0.0.0.0:80
prod:
	./app.py '$(APP_ID)' '$(SECRET_KEY)'

# Запускаем на деве 127.0.0.1:8080
dev:
	./app.py '$(APP_ID)' '$(SECRET_KEY)' 'test'

install:
	pip3 install -r requirements.txt --upgrade pip
