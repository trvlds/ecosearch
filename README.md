# ecosearch

Поисковая платформа на русском и английском языках с поддержкой голосового ввода для поиска экологических данных

![demo](https://i.imgur.com/32GDJfH.gif)

## Примеры запросов (на английском)

* [Earth Sciences questions](markdown/earth_sciences.md)
* [Weather & Meteorology questions](markdown/weather_and_meteorology.md)

## Как запустить приложение?

Идем на [WolframAlpha](https://developer.wolframalpha.com/portal/signin.html), создаем аккаунт и получаем `AppID` для своего приложения, например  `EEEFFF-CCDDEEFFGG`

Обновляем систему, открываем порт `80` на фаерволле и устанавливаем пакеты:
```bash
yum update -y
yum install epel-release -y
yum install make git python34 python34-pip -y

firewall-cmd --zone=public --add-port=80/tcp --permanent
firewall-cmd --reload
```

Клонируем репозиторий и устанавливаем зависимости:
```bash
git clone https://bitbucket.com/trvlds/ecosearch /srv/ecosearch/; cd /srv/ecosearch/
make install
```

В конфигурационный файл записываем полученный `AppID`:
```
vim Makefile
APP_ID=EEEFFF-CCDDEEFFGG
```

Запуск:
```bash
cd /srv/ecosearch/
make prod
./app.py 'EEEFFF-CCDDEEFFGG' 'AKb8454ZvVMz18Wwb6Sf'
 * Running on http://0.0.0.0:80/ (Press CTRL+C to quit)
```

## TODO

* [Голосовая поддержка](https://developer.mozilla.org/ru/docs/Web/API/SpeechRecognition#Browser_compatibility) работает только для браузеров на webkit
* Телеграм бот
* Перевод контента из вида картинок в текстовый
* Тесты для деплоя (TravisCI)
* Демо на хостинге
* Страницы для версий для печати

## Лицензия

[LICENSE.md](LICENSE.md)