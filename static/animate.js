// Make animated gear icon after search button pressed

var showGear = document.getElementById('loading');
var searchButton = document.getElementById('search');
searchButton.addEventListener('click', buttonClick);

function buttonClick(event) {
    updateUi();
}

function updateUi() {
    showGear.style.display = 'block';
}
