// Make voice speech for webkit-based desktop browsers

try {
    var SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition;
    var recognition = new SpeechRecognition();
}
catch(e) {
    console.error(e);
    $('.no-browser-support').show();
    $('.app').hide();
}

recognition.onresult = function(event) {
    var current = event.resultIndex;
    var transcript = event.results[current][0].transcript;
    var mobileRepeatBug = (current == 1 && transcript == event.results[0][0].transcript);
    if(!mobileRepeatBug)
        $('#query').val(transcript);
};

var toggleStatus = true;
$('.lang').click(function() {
    if (this.id == 'ru')
        recognition.lang = 'RU';
    else
        recognition.lang = 'en-US';
    if (toggleStatus)
        recognition.start();
    else
        recognition.stop();
    toggleStatus = !toggleStatus;
});

function readOutLoud(message) {
    var speech = new SpeechSynthesisUtterance();
    speech.text = message;
    speech.volume = 1;
    speech.rate = 1;
    speech.pitch = 1;
    window.speechSynthesis.speak(speech);
}
