#!/usr/bin/env python3

from flask import Flask, render_template, flash, request
from wtforms import Form, TextField, validators
from alphabet_detector import AlphabetDetector
from translate import Translator
from urllib.request import Request, urlopen
from xml.dom import minidom
from sys import argv

app = Flask(__name__)
app.config.from_object(__name__)

try:
    appid = str(argv[1])
    apilink = 'https://api.wolframalpha.com/v2/query?appid={}'.format(appid)
    app.config['SECRET_KEY'] = str(argv[2])
except(IndexError):
    print('Set correct WolframAlpha AppID')
    exit(1)

if len(argv) is 4 and str(argv[3]) == 'test':
    app.debug = True


class ReusableForm(Form):
    query = TextField('Search',
        validators=[
            validators.required(),
            validators.Length(min=1, max=100)
        ])


@app.route('/', methods=['GET', 'POST'])
def main():
    form = ReusableForm(request.form)
    if form.errors:
        print('Error: {}'.format(form.errors))

    if request.method == 'POST':
        query = request.form['query']
        print('Query: {}'.format(query))

        # Detect russian
        ad = AlphabetDetector()
        result = ad.only_alphabet_chars(query, 'LATIN')
        if not result:
            translator = Translator(from_lang='ru', to_lang='en')
            query = translator.translate(query)
            print('Translated query: {}'.format(query))

        # Get data
        url = '{0}&input={1}'.format(apilink, query.replace(' ', '%20'))
        req = Request(url)
        try:
            data = urlopen(req).read().decode('utf-8')
        except(UnicodeEncodeError):
            print('Can\'t search by this query. Wrong symbols detected')
            flash('Try another query', 'empty')
            return render_template('index.html', form=form)

        # Parse XML
        result = ''
        xmldoc = minidom.parseString(data)

        status = xmldoc.getElementsByTagName('queryresult')
        pod = xmldoc.getElementsByTagName('pod')
        didyoumeans = xmldoc.getElementsByTagName('didyoumeans')

        for i in status:
            status_success = i.attributes['success'].value
            status_error = i.attributes['error'].value
            numpods = i.attributes['numpods'].value

        if status_success == 'true' and status_error == 'false':
            if int(numpods) is 0:
                result = 'Try to search by another query'
            else:
                for i in pod:
                    pod_title = i.attributes['title'].value
                    pod_error = i.attributes['error'].value
                    if pod_error == 'false':
                        result += '<h3>{0}:</h3>\n'.format(pod_title)
                    subpod = i.getElementsByTagName('subpod')
                    for j in subpod:
                        subpod_title = j.attributes['title'].value
                        if subpod_title != '':
                            result += '<h4>{0}:</h4>\n'.format(subpod_title)
                        img = j.getElementsByTagName('img')
                        for k in img:
                            src = k.attributes['src'].value
                            width = k.attributes['width'].value
                            height = k.attributes['height'].value
                            result += '<img src=\"{0}\" width=\"{1}\" height=\"{2}\">\n' \
                                .format(src, width, height)
                        result += ''
        else:
            if didyoumeans:
                for i in didyoumeans:
                    subpod = i.getElementsByTagName('didyoumean')[0]
                    means = subpod.firstChild.data
            else:
                result = 'No results for query'

        # Validation
        if form.validate():
            flash(query, 'query')
            try:
                flash(means, 'didyoumean')
            except(NameError):
                flash(result, 'result')
                global printmsg
                printmsg = result
        else:
            flash('You need to write something', 'empty')

    return render_template('index.html', form=form)


@app.route('/print', methods=['GET'])
def print_info():
    print('Printing from html')
    return render_template('print.html', printmsg=printmsg)


@app.errorhandler(404)
def page_not_found(e):
    return render_template('error.html', error_code=404), 404


@app.errorhandler(500)
def internal_error(e):
    return render_template('error.html', error_code=500), 500


if __name__ == '__main__':
    if app.debug:
        app.run(host='127.0.0.1', port=8080)
    else:
        app.run(host='0.0.0.0', port=80)
